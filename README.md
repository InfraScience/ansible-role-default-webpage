Role Name
=========

A role that installs a static webpage, optionally redirecting to one or more services.

Role Variables
--------------

The most important variables are listed below:

``` yaml
web_document_root: '/var/www//html'
web_page_title: "D4Science Service"
#d4science_running_services: []
#  - { url: 'service', desc: 'service description' }
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
